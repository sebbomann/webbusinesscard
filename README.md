
[![pipeline status](https://gitlab.com/sebbomann/webbusinesscard/badges/master/pipeline.svg)](https://gitlab.com/sebbomann/webbusinesscard/-/commits/master)

# Webbusinesscard

This repository holds my webbusinesscard. Thw website itself and all
configuration was built by scratch, to showcase some modern frontend and devops
techniques.

## Development and Usage

To develop you would need to install a recent version of yarn. You can follow
these
[instructions](https://classic.yarnpkg.com/en/docs/install/#debian-stable), if
you like.

After yarn is installed, you can install all the needed packages in the project
structure with:

```
yarn install
```

After all the packages are installed, you can run the dev server:

```
yarn serve
```

A new browser tab with the webbusinesscard should now be opened. You can edit
the webbusinesscard now and directly see the results of editing the source.

If you are happy with your changes, pack them in git commits and push them back
to GitLab.

## CI/CD

When changes are pushed to GitLab, the
(https://gitlab.com/sebbomann/webbusinesscard/-/pipelines)[CI/CD pipelines] are
started. One job uses webpack to bundle the page together, The other job builds
a docker container based on caddy to serve the bundled content.

At the end of the second job, the newly built docker image is pushed to the
(https://gitlab.com/sebbomann/webbusinesscard/container_registry)[GitLab container registry] and can now be deployed by sebbomann/webhosting>.

## TODO

- Configure caddy to use gzip
- Notify sebbomann/webhosting> to autodeploy the new docker container on a
  server

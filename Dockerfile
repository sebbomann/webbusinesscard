FROM caddy:2.7

# Configure caddy
COPY Caddyfile /etc/caddy/Caddyfile
RUN caddy fmt --overwrite /etc/caddy/Caddyfile
RUN caddy validate --config /etc/caddy/Caddyfile

# Copy content
COPY dist /srv
